﻿using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Helper;
using MMDB.MovieDatabase.ValueObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MMDB3
{
    class SearchResultItemToImagePathConverter : IValueConverter
    {
        private const string imageDirectoryName = "Resources";
        private const string actorImageName = "actor.png";
        private const string actor_directorImageName = "actor_director.png";
        private const string directorImageName = "director.png";
        private const string movieImageName = "movie.png";
        private const string unknownImageName = "unknown.png";
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string path;
                switch ((SearchResultItemType)value)
                {
                    case SearchResultItemType.None:
                        if (TryGetFilePath.InProjectDirectory(unknownImageName, imageDirectoryName, true, out path))
                        {
                            return path;
                        }
                        return null;
                    case SearchResultItemType.Movie:
                        if (TryGetFilePath.InProjectDirectory(movieImageName, imageDirectoryName, true, out path))
                        {
                            return path;
                        }
                        return null;
                    case SearchResultItemType.Actor:
                        if (TryGetFilePath.InProjectDirectory(actorImageName, imageDirectoryName, true, out path))
                        {
                            return path;
                        }
                        return null;
                    case SearchResultItemType.Director:
                        if (TryGetFilePath.InProjectDirectory(directorImageName, imageDirectoryName, true, out path))
                        {
                            return path;
                        }
                        return null;
                    case SearchResultItemType.ActorDirector:
                        if (TryGetFilePath.InProjectDirectory(actor_directorImageName, imageDirectoryName, true, out path))
                        {
                            return path;
                        }
                        return null;
                }
            }

            throw new Exception($"Can not convert {value.GetType().ToString()} to file path.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.ValueObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MMDB3
{
    class SearchResultItemToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value is MovieSearchResultItem)
                {
                    string movieString = (value as MovieSearchResultItem).ToString();
                    return movieString.Substring(movieString.IndexOf(':') + 2);
                }
                if (value is CastOrCrewSearchResultItem)
                {
                    string castOrCrewString = (value as CastOrCrewSearchResultItem).ToString();
                    return castOrCrewString.Substring(castOrCrewString.IndexOf(':') + 2);
                }
                //string ResultItemString = (value as SearchResultItem).ResultItem.ToString();
                //return ResultItemString.Substring(ResultItemString.IndexOf(':') + 2);
            }

            throw new Exception($"Can not convert {value.GetType().ToString()} to string.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

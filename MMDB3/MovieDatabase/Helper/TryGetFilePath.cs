﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMDB.MovieDatabase.Helper
{
    public class TryGetFilePath
    {
        private static string _AppDomainPath = AppDomain.CurrentDomain.BaseDirectory;
        private static string _AssemblyPath = new Uri(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).LocalPath;
        private static string _DirectoryPath = System.IO.Directory.GetCurrentDirectory();

        /// <summary>
        /// Tries to create a file path. Option: checks if the file exits.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool InProjectDirectory(string fileName, bool checkIfFileExists, out string path)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                path = null;
                return false;
            }

            path = _AppDomainPath;

            for (int i = 0; i < ((path == _DirectoryPath) ? 2 : 3); i++)
            {
                path = Path.GetDirectoryName(path);
            }

            path = Path.Combine(path, fileName);

            if (checkIfFileExists)
            {
                if (File.Exists(path))
                {
                    return true;
                }
            }

            path = null;
            return false;
        }

        public static bool InProjectDirectory(string fileName, string subDirectory, bool checkIfFileExists, out string path)
        {
            if (string.IsNullOrWhiteSpace(subDirectory))
            {
                path = null;
                return false;
            }

            if (!InProjectDirectory(Path.Combine(subDirectory, fileName), checkIfFileExists, out path))
            {
                path = null;
                return false;
            }

            return true;
        }

        public static bool InProjectDirectory(string fileName, string[] subDirectories, bool checkIfFileExists, out string path)
        {
            if (subDirectories == null || subDirectories.Length == 0)
            {
                path = null;
                return false;
            }
            foreach (string subDirectory in subDirectories)
            {
                if (string.IsNullOrWhiteSpace(subDirectory))
                {
                    path = null;
                    return false;
                }
            }

            if (!InProjectDirectory(Path.Combine(Path.Combine(subDirectories), fileName), checkIfFileExists, out path))
            {
                path = null;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Tries to create a file path. Option: checks if the file exits.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool InSolutionDirectory(string fileName, bool checkIfFileExists, out string path)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                path = null;
                return false;
            }

            path = _AppDomainPath;

            for (int i = 0; i < ((path == _DirectoryPath) ? 3 : 4); i++)
            {
                path = Path.GetDirectoryName(path);
            }

            path = Path.Combine(path, fileName);

            if (checkIfFileExists)
            {
                if (File.Exists(path))
                {
                    return true;
                }
            }

            path = null;
            return false;
        }

        public static bool InSolutionDirectory(string fileName, string subDirectory, bool checkIfFileExists, out string path)
        {
            if (string.IsNullOrWhiteSpace(subDirectory))
            {
                path = null;
                return false;
            }

            if (!InSolutionDirectory(Path.Combine(subDirectory, fileName), checkIfFileExists, out path))
            {
                path = null;
                return false;
            }

            return true;
        }

        public static bool InSolutionDirectory(string fileName, string[] subDirectories, bool checkIfFileExists, out string path)
        {
            if (subDirectories == null || subDirectories.Length == 0)
            {
                path = null;
                return false;
            }
            foreach (string subDirectory in subDirectories)
            {
                if (string.IsNullOrWhiteSpace(subDirectory))
                {
                    path = null;
                    return false;
                }
            }

            if (!InSolutionDirectory(Path.Combine(Path.Combine(subDirectories), fileName), checkIfFileExists, out path))
            {
                path = null;
                return false;
            }

            return true;
        }
    }
}

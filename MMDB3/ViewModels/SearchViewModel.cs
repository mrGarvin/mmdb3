﻿using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Services;
using MMDB.MovieDatabase.ValueObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MMDB3
{
    public class SearchViewModel : INotifyPropertyChanged
    {
        private CastOrCrewService castOrCrewService;
        private MovieService movieService;
        private SearchService searchService;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public string SearchText
        {
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    SearchResultItems = null;
                    SelectedSearchResultItem = null;
                }
                else
                {
                    SearchResultItems = new ObservableCollection<SearchResultItem>(searchService.Search(value, true));
                }
            }
        }
        public ObservableCollection<SearchResultItem> SearchResultItems
        {
            get { return _searchResultItems; }
            set { SetField(ref _searchResultItems, value); }
        }
        private ObservableCollection<SearchResultItem> _searchResultItems;
        public SearchResultItem SelectedSearchResultItem
        {
            get
            {
                return _selectedSearchResultItem;
            }
            set
            {
                if (value != null)
                {
                    if (value.ResultItem is Movie)
                    {
                        Header_Label = $"{(value.ResultItem as Movie).Title} ({(value.ResultItem as Movie).ProductionYear.ToString()})";
                        if (movieService.GetDirectors(value.ResultItem as Movie).Count() > 0)
                        {
                            Directors_Label = "Directors:";
                            Directors_TextBlock = iEnumerableToString(movieService.GetDirectors(value.ResultItem as Movie));
                            Directors_IsVisible = true;
                        }
                        else
                        {
                            Directors_IsVisible = false;
                        }
                        if (movieService.GetActors(value.ResultItem as Movie).Count() > 0)
                        {
                            Actors_Label = "Actors:";
                            Actors_IsVisible = true;
                            Actors_TextBlock = iEnumerableToString(movieService.GetActors(value.ResultItem as Movie));
                        }
                        else
                        {
                            Actors_IsVisible = false;
                        }
                    }
                    else if (value.ResultItem is CastOrCrew)
                    {
                        Header_Label = $"{(value.ResultItem as CastOrCrew).Name} ({(value.ResultItem as CastOrCrew).DateOfBirth.ToShortDateString()})";
                        if (castOrCrewService.GetDirectedMovies(value.ResultItem as CastOrCrew).Count() > 0)
                        {
                            Directors_Label = "Directed movies:";
                            Directors_TextBlock = iEnumerableToString(castOrCrewService.GetDirectedMovies(value.ResultItem as CastOrCrew));
                            Directors_IsVisible = true;
                        }
                        else
                        {
                            Directors_IsVisible = false;
                        }
                        if (castOrCrewService.GetActedMovies(value.ResultItem as CastOrCrew).Count() > 0)
                        {
                            Actors_Label = "Acted in movies:";
                            Actors_TextBlock = iEnumerableToString(castOrCrewService.GetActedMovies(value.ResultItem as CastOrCrew));
                            Actors_IsVisible = true;
                        }
                        else
                        {
                            Actors_IsVisible = false;
                        }
                    }
                }
                else
                {
                    resetSelectedSearchResultItem();
                }

                //_selectedSearchResultItem = value;
                //OnPropertyChanged(nameof(SelectedSearchResultItem));
                SetField(ref _selectedSearchResultItem, value);
            }
        }
        private SearchResultItem _selectedSearchResultItem;
        public string ImagePath
        {
            get { return _imagePath; }
            set { SetField(ref _imagePath, value); }
        }
        private string _imagePath;
        public string Header_Label
        {
            get { return _header_label; }
            set { SetField(ref _header_label, value); }
        }
        private string _header_label;
        public string Directors_Label
        {
            get { return _directors_Label; }
            set { SetField(ref _directors_Label, value); }
        }
        private string _directors_Label;
        public bool Directors_IsVisible
        {
            get { return _directors_isVisible; }
            set { SetField(ref _directors_isVisible, value); }
        }
        private bool _directors_isVisible;
        public string Directors_TextBlock
        {
            get { return _directors_TextBlock; }
            set { SetField(ref _directors_TextBlock, value); }
        }
        private string _directors_TextBlock;
        public string Actors_Label
        {
            get { return _actors_Label; }
            set { SetField(ref _actors_Label, value); }
        }
        private string _actors_Label;
        public bool Actors_IsVisible
        {
            get { return _actors_isVisible; }
            set { SetField(ref _actors_isVisible, value); }
        }
        private bool _actors_isVisible;
        public string Actors_TextBlock
        {
            get { return _actors_TextBlock; }
            set { SetField(ref _actors_TextBlock, value); }
        }
        private string _actors_TextBlock;

        public SearchViewModel()
        {
            castOrCrewService = new CastOrCrewService();
            movieService = new MovieService();
            searchService = new SearchService();
        }

        private string iEnumerableToString(IEnumerable<object> collection)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (collection is IEnumerable<Movie>)
            {
                List<Movie> movieList = (collection as IEnumerable<Movie>).ToList();
                for (int i = 0; i < (movieList.Count - 1); i++)
                {
                    stringBuilder.AppendLine($"{movieList[i].Title} ({movieList[i].ProductionYear.ToString()})");
                }
                stringBuilder.Append($"{movieList[movieList.Count - 1].Title} ({movieList[movieList.Count - 1].ProductionYear.ToString()})");
            }
            else if (collection is IEnumerable<CastOrCrew>)
            {
                List<CastOrCrew> castOrCrewList = (collection as IEnumerable<CastOrCrew>).ToList();
                for (int i = 0; i < (castOrCrewList.Count - 1); i++)
                {
                    stringBuilder.AppendLine($"{castOrCrewList[i].Name} ({castOrCrewList[i].DateOfBirth.ToShortDateString()})");
                }
                stringBuilder.Append($"{castOrCrewList[castOrCrewList.Count - 1].Name} ({castOrCrewList[castOrCrewList.Count - 1].DateOfBirth.ToShortDateString()})");
            }
            else
            {
                return null;
            }

            return stringBuilder.ToString();
        }

        private List<string> iEnumerableToListOfStrings(IEnumerable<SearchResultItem> listOfItems)
        {
            if (listOfItems != null)
            {
                List<string> listOfStrings = new List<string>();

                foreach (SearchResultItem item in listOfItems)
                {
                    listOfStrings.Add(item.ToString().Substring(item.ToString().IndexOf(':') + 2));
                }

                return listOfStrings;
            }

            return null;
        }

        private void resetSelectedSearchResultItem()
        {
            Header_Label = null;

            Directors_Label = null;
            Directors_TextBlock = null;
            Directors_IsVisible = false;

            Actors_Label = null;
            Actors_TextBlock = null;
            Actors_IsVisible = false;
        }

        //protected void OnPropertyChanged(string name)
        //{
        //    //PropertyChangedEventHandler handler = PropertyChanged;
        //    //if (handler != null)
        //    //{
        //    //    handler(this, new PropertyChangedEventArgs(name));
        //    //}
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        //}
    }
}
